(define (S n z)
	  (define (iter n z)
	    (if (zero? n)
	        z
	        (iter (- n 1) (+ z 1))))
	  (iter n z))
	  
(define Add(lambda (a b)         
		(S a (S b 0)) ))